import re
from pathlib import Path
from typing import Callable, Dict, Generator, Iterable, List, Optional, Pattern


BASE_DIR = Path(__file__).parents[1]
DATA_DIR = f'{BASE_DIR}/data'


def read_file_logs(file: str) -> Generator[str, None, None]:
    with open(file, 'r', encoding='utf-8') as f:
        for line in f:
            yield line


def execute_query(cmd: str, value: str, data: Optional[Iterable[str]],
        file_name: str) -> List[str]:
    path: str = f'{DATA_DIR}/{file_name}'
    if data is None:
        data_: Iterable[str] = read_file_logs(path)
    else:
        data_ = data

    return list(CMD[cmd](value, data_))


def filter_query(value:str, data: List[str]) -> List[str]:
    return list(filter(lambda v: value in v, data))


def map_query(value: str, data: List[str]) -> List[str]:
    return list(map(lambda text: text.split(' ')[int(value)], data))


def unique_query(data: List[str]) -> List[str]:
    return list(set(data))


def sort_query(value: str, data: List[str]) -> List[str]:
    return sorted(data, reverse=True if value == 'desc' else False)


def limit_query(value: str, data: List[str]) -> List[str]:
    return data[:int(value)]


def regex_query(value: str, data: List[str]) -> List[str]:
    pattern: Pattern[str] = re.compile(value)
    return filter(lambda x: re.search(pattern, x), data)


CMD: Dict[str, Callable] = {
    'filter': filter_query,
    'map': map_query,
    'unique': unique_query,
    'sort': sort_query,
    'limit': limit_query,
    'regex': regex_query,
}

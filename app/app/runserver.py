from flask import Flask, Response, request, jsonify
from marshmallow import ValidationError


from app.models import RequestSchema
from utils import execute_query


app: Flask = Flask(__name__)


@app.post('/perform_query')
def perform_query() -> Response:
    data = request.json
    try:
        RequestSchema().load(data)
    except ValidationError as error:
        return jsonify(error.messages), 400
    first_result = execute_query(
        cmd=data['cmd1'],
        value=data['value1'],
        data=None,
        file_name=data['file_name']
    )
    result = execute_query(
        cmd=data['cmd2'],
        value=data['value2'],
        data=first_result,
        file_name=data['file_name']

    )
    return jsonify(result)


if __name__ == '__main__':
    app.run(debug=True)
